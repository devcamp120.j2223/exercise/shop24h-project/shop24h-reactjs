import {createStore, combineReducers} from "redux"
import taskEven from "../component/Producet/filterReducer";

const appReducer = combineReducers({
    taskReducer: taskEven

});
const store = createStore(
    appReducer,
    {},
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
 export default store