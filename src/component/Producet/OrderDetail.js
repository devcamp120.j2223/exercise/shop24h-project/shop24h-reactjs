
import { Col, Row } from "reactstrap";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Button, Typography } from '@mui/material';
import { useEffect, useState } from "react"

const OrderDetail = ({ nameProp, priceProp, img, buyProp, id }) => {

    return (
       
            <Col className="col-sm-4 p-3">
                <Card sx={{ width: 150, textAlign: "center", boxShadow: "none" }}>
                    <a href={"/product/" + id} >
                        <CardMedia
                            component="img"
                            height="200"
                            image={img}
                            alt="green iguana"
                        />
                    </a>

                    <CardContent>
                        <Typography gutterBottom variant="h6" component="div">
                            <b>
                                {nameProp}
                            </b>
                        </Typography>
                        <Row>
                            <Typography variant="h8" color="text.secondary">
                                <strike style={{ color: "red" }}> {buyProp}</strike> &nbsp; {priceProp}  USD
                            </Typography>
                        </Row>
                    </CardContent>
                </Card>
            </Col>

     

    )
}

export default OrderDetail;