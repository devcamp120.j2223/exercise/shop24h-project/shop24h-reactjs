
import NotificationsSharpIcon from '@mui/icons-material/NotificationsSharp';
import AddShoppingCartSharpIcon from '@mui/icons-material/AddShoppingCartSharp';
import { Grid, Typography } from "@mui/material"
function IconShop() {
    return (
        <Typography variant="h5" sx={{ flexGrow: 1, }}>
            <Grid>
                < NotificationsSharpIcon  sx={{color:"#fffe00"}}/>&nbsp; 
               
                < AddShoppingCartSharpIcon sx={{color:"#fffe00"}}/> &nbsp; 
            </Grid>
        </Typography>
    )
}
export default IconShop