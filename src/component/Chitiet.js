import { Container, Grid, CardMedia, CardContent, Typography, Button, Rating, FormControlLabel, Pagination } from "@mui/material"
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom"
import { Row, Col } from "reactstrap";

import OrderDetail from "./Producet/OrderDetail";


function ProductDitail() {

    const { id } = useParams()
    const [porduct, setProduct] = useState({});
    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
    useEffect((data) => {

        fetchAPI("http://localhost:8000/product/" + id)
            .then((data) => {
                setProduct(data.data)
                console.log(data);
            })
            .catch((error) => {
                console.error(error.message);
            })
    }, []);
    // danh mục san phẩm
    const [clockList, setCloclName] = useState([])
    const [limit, setLimit] = useState(6);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const changePageHandler = (event, value) => {
        setPage(value)
    }

    useEffect((data) => {
        fetchAPI("http://localhost:8000/product")
            .then((data) => {
                setNoPage(Math.ceil(data.data.length / limit));

                setCloclName(data.data.slice((page - 1) * limit, page * limit));
                console.log(data);
            })
            .catch((error) => {
                console.error(error.message);
            })
    }, [limit, page]);

    return (
        <Container >
            
            <Row style={{ textAlign: "center" }} mt={5}>
                < Col>
                    <Grid container style={{ textAlign: "center", justifyContent: "center" }}>
                        <Grid item>
                            <CardMedia
                                component="img"
                                height="300"
                                image={porduct.imageUrl}
                                alt="green iguana"
                            />
                        </Grid>
                        <Grid>
                            < CardContent   >
                                <Typography sx={{ textAlign: "center" }} variant="h4" color="red" gutterBottom >{porduct.name}</Typography>
                                <Grid mt={2}>
                                    <Typography variant="h6">Giá: {porduct.promotionPrice} USD </Typography>
                                </Grid>
                                <Grid mt={2}>
                                    <Typography variant="h6">Bảo hành: 12 tháng </Typography>
                                </Grid>
                                <Grid container mt={2}>
                                    <Typography variant="h6">Đánh giá:</Typography>
                                    <Rating name="half-rating" defaultValue={4} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                                <Grid container mt={2}>
                                    <Typography variant="h6">Tình trạng: Còn hàng</Typography>

                                </Grid>
                                <Grid container mt={2}>
                                    <Grid>
                                        <Button sx={{ textAlign: "center", background: "#2196f3", color: "black" }} >Mua</Button>
                                    </Grid>
                                    <Grid>
                                        <Button mt={2} sx={{ textAlign: "center", background: "red", color: "black" }} >Liên hệ</Button>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Grid>
                    </Grid>
                    <Grid>
                        <p>“Đồng hồ điện tử được biết đến là sản phẩm “độc quyền” đến từ thương hiệu A. watch. Nhờ thiết kế trẻ trung cùng giá
                            thành rẻ, đồng hồ điện tử chính hãng mặc nhiên trở thành sản phẩm ăn khách nhất và dẫn đầu về số lượng người
                            dùng không chỉ tại Việt Nam. Trên thực tế, sức “NÓNG” của dòng đồng hồ điện tử giá rẻ còn phụ thuộc vào hàng
                            loạt tính năng độc đáo được tích hợp riêng cho mỗi phiên bản. Điều này đã mang đến trải nghiệm mới hơn và nếu
                            chỉ có trên dưới 1 triệu đồng mà muốn sở hữu đồng hồ hiệu, giới trẻ chỉ có thể tìm đến đồng hồ điện tử mà thôi.”
                        </p>
                    </Grid>
                    <Grid>
                        <Grid >
                            <Grid container>
                                {clockList.map((clock, index) => {
                                    return (
                                        <OrderDetail key={index} nameProp={clock.name} priceProp={clock.promotionPrice} img={clock.imageUrl} buyProp={clock.buyPrice} id={clock._id} />
                                    )
                                })}
                            </Grid>
                        </Grid >
                    </Grid>
                </Col>
            </Row>
            <Grid container justifyContent="end">
                <Grid item>
                    <Pagination color="secondary"
                        count={noPage} defaultPage={page} onChange={changePageHandler}>
                    </Pagination>
                </Grid>
            </Grid>
        </Container>
    )
}
export default ProductDitail