import { Box, FormGroup, AppBar, Toolbar, Grid, Typography } from "@mui/material"
import Dt from '@mui/icons-material/AddIcCall';
import FacebookIcon from '@mui/icons-material/Facebook';
import CameraAltIcon from '@mui/icons-material/CameraAlt';

function Footer() {
    return (
        <div  >
            <Grid item mt={5}>
                <Box sx={{ flexGrow: 1, height: "150px" }}>
                    <FormGroup color="black">
                        <AppBar position="static" style={{background: "#3f51b5", height: "150px"}}>
                            <Toolbar>
                                <Typography variant="h9" sx={{ flexGrow: 1, textAlign: "center", color: "#ffeb3b" }} mt={5}>
                                    CHĂM SÓC<br/>
                                    hướng dẫn<br/>
                                    chính sách<br/>
                                    tra cứu<br/>
                                </Typography>
                                <Typography variant="h9" sx={{ flexGrow: 1, textAlign: "center", color: "#ffeb3b", display: "flex" }} mt={5}>
                                    TỔNG ĐÀI<br/>
                                    Gọi mua: 1008<br/>
                                    Kỹ thuật: 1009<br/>
                                    Bảo hàng : 2000<br/>
                                </Typography>
                                <Typography textAlign = "center" mt={3} sx={{ flexGrow: 1, color: "#ffeb3b", display: "inline-flex", alignItems: "center" }}>
                                <h2 style={{color: "red"}}>AT.Wacth  </h2><br/>
                                <Dt/> <FacebookIcon/> <CameraAltIcon/>
                                </Typography>
                            </Toolbar>
                        </AppBar >
                    </FormGroup>
                </Box>
            </Grid >
        </div>
    )
}
export default Footer