
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Grid, Pagination, Typography, Button,  } from '@mui/material';
import OrderDetail from '../../Producet/OrderDetail';
import { useEffect, useState } from "react"
import { Row, Col } from 'react-bootstrap';

import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
function LastestProducts() {
    const [clockList, setCloclName] = useState([])
    const [limit, setLimit] = useState(9);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);

    //Pagination
    const changePageHandler = (event, value) => {
        setPage(value)
    }

    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }


    useEffect((data) => {
        fetchAPI("http://localhost:8000/product")
            .then((data) => {
                setNoPage(Math.ceil(data.data.length / limit));

                setCloclName(data.data.slice((page - 1) * limit, page * limit));
                console.log(data);
            })
            .catch((error) => {
                console.error(error.message);
            })
    }, [limit, page]);
    return (
        <Grid mt={7}>
            
                <Row>
                    {clockList.map((clock, index) => {
                        return (
                            <OrderDetail key={index} nameProp={clock.name} priceProp={clock.promotionPrice} img={clock.imageUrl} buyProp={clock.buyPrice} id={clock._id} />
                        )
                    })}
                </Row>
           
            <Grid container justifyContent="end">
                    <Grid item>
                        <Pagination color="secondary"
                            count={noPage} defaultPage={page} onChange={changePageHandler}>
                        </Pagination>
                    </Grid>
            </Grid>

            <Grid mt={5} textAlign="center">
                <Button variant='contained' href='/product'>
                    VIEW ALL
                </Button>
            </Grid>
        </Grid>
    );
}

export default LastestProducts;